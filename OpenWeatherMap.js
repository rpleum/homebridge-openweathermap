"use strict";
const http = require("http");

module.exports = {
	getTemperature: function(latitude, longitude, apiKey, callback)
	{
		const requestUrl = 'http://api.openweathermap.org/data/2.5/weather?lat=' + latitude + '&lon=' + longitude + '&units=metric' + '&appid=' + apiKey;
		http.get(requestUrl, (res) =>
		{
			res.setEncoding('utf8');
			let rawData = '';
			res.on('data', (chunk) => rawData += chunk);
			res.on('end', () =>
			{
				var object = JSON.parse(rawData);
				var temperature = object.main.temp;
				callback(null, temperature);
			});
			res.on('error', (e) =>
			{
				callback(e, null);
			});
		});
	},
	
	getHumidity: function(latitude, longitude, apiKey, callback)
	{
		const requestUrl = 'http://api.openweathermap.org/data/2.5/weather?lat=' + latitude + '&lon=' + longitude + '&appid=' + apiKey;
		http.get(requestUrl, (res) =>
		{
			res.setEncoding('utf8');
			let rawData = '';
			res.on('data', (chunk) => rawData += chunk);
			res.on('end', () =>
			{
				var object = JSON.parse(rawData);
				var humidity = object.main.humidity;
				callback(null, humidity);
			});
			res.on('error', (e) =>
			{
				callback(e, null);
			});
		});
	},
}
