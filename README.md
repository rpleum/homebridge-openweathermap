# homebridge-openweathermap #

A homebridge plugin that reads weather information via openweathermap.org.

## Installation ##

1. Install Homebridge using: npm install -g homebridge
2. Install this plugin using: npm install -g homebridge-openweathermap
3. Update your Homebridge config.json using the sample below.

## Configuration ##


```
{
	"accessory": "OpenWeatherMap-Temperature",
	"name": "Temperature",
	"latitude": "40.730610",
	"longitude": "-73.935242",
	"apiKey": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"	
},
{
	"accessory": "OpenWeatherMap-Humidity",
	"name": "Humidity",
	"latitude": "40.730610",
	"longitude": "-73.935242",
	"apiKey": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"		
},
```

Fields:

* **accessory**: Must be either "OpenWeatherMap-Temperature" or "OpenWeatherMap-Humidity".

* **name**: User defined name of the device (will be used for logging).

* **latitude**: Latitude GPS coordinate of the location of your interest.

* **longitude**: Longitude GPS coordinate of the location of your interest.

* **apiKey**: Your unique API key for openweathermap.org. See http://openweathermap.org/appid
