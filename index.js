"use strict";
const openWeatherMap = require('./OpenWeatherMap.js');

var Service, Characteristic;

module.exports = function (homebridge)
{
	Service = homebridge.hap.Service;
	Characteristic = homebridge.hap.Characteristic;
	homebridge.registerAccessory('homebridge-openweathermap', 'OpenWeatherMap-Temperature', TemperatureAccessory);
	homebridge.registerAccessory('homebridge-openweathermap', 'OpenWeatherMap-Humidity', HumidityAccessory);
}

function TemperatureAccessory(log, config)
{
	this.log = log;
	this.name = config['name'];
	this.latitude = config['latitude'];
	this.longitude = config['longitude'];
	this.apiKey = config['apiKey'];
}

function HumidityAccessory(log, config)
{
	this.log = log;
	this.name = config['name'];
	this.latitude = config['latitude'];
	this.longitude = config['longitude'];
	this.apiKey = config['apiKey'];
}

TemperatureAccessory.prototype =
{
	identify: function (callback)
	{
		this.log('Identify requested!');
		callback();
	},

	getServices: function ()
	{
		var informationService = new Service.AccessoryInformation();
		informationService
		  .setCharacteristic(Characteristic.Manufacturer, 'OpenWeatherMap')
		  .setCharacteristic(Characteristic.Model, 'Temperature');
		
		const temperatureService = new Service.TemperatureSensor(this.name);
		temperatureService
		.getCharacteristic(Characteristic.CurrentTemperature)
		.on('get', (callback) => {
			this.log('Retrieving temperature from OpenWeatherMap');
			openWeatherMap.getTemperature(this.latitude, this.longitude, this.apiKey, (error, temperature) =>
			{
				this.log('Temperature is: ' + temperature + '\'C');
				callback(error, temperature);
			});
		});
		
		return [informationService, temperatureService];
	}
};


HumidityAccessory.prototype =
{
	identify: function (callback)
	{
		this.log('Identify requested!');
		callback();
	},

	getServices: function ()
	{
		var informationService = new Service.AccessoryInformation();
		informationService
		  .setCharacteristic(Characteristic.Manufacturer, 'OpenWeatherMap')
		  .setCharacteristic(Characteristic.Model, 'Humidity');
		
		const humidityService = new Service.HumiditySensor(this.name);
		humidityService
		.getCharacteristic(Characteristic.CurrentRelativeHumidity)
		.on('get', (callback) => 
		{
			this.log('Retrieving humidity from OpenWeatherMap');
			openWeatherMap.getHumidity(this.latitude, this.longitude, this.apiKey, (error, humidity) =>
			{
				this.log('Humidity is: ' + humidity + '%' );
				callback(error, humidity);
			});
		});		
		
		return [informationService, humidityService];
	}
};